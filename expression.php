<?php
$result = 3 % 2;
echo $result;
echo "<br>";
var_dump(3.0 === 3);
echo "<br>";
$str1 = "Hello";
$str2 = "World";

$str = $str1 ."\n". $str2;
// $str = nl2br($str);
echo $str;
echo "<br>";

$arr1 = array(1, 11, 23, 412);
$arr2 = array(101, 101, 203, 42);
$arr3 = array(121, 110, 233, 422);
$arr4 = array(111, 131, 230, 420);

echo "<pre>";
var_dump($arr1 !== $arr2);
echo "</pre>"; 